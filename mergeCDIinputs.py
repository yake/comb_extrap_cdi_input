from collections import defaultdict
import re

def read_extrap_file(input_dir, f60, f70, f77, f85, f100):
    # uncs["pt"]["WP"][0/1] : 0 centralvalue, 1 uncs
    uncs = defaultdict(lambda: defaultdict(dict))
    fs = [f60, f70, f77, f85, f100]
    tag_weights = ["FixedCutBEff_60","FixedCutBEff_70","FixedCutBEff_77","FixedCutBEff_85","FixedCutBEff_100"]
    unc_name_set = set()
    for tag_weight, f_wp in zip(tag_weights, fs):
        tag_weight = convert_WP(tag_weight)
        temp_f = open(input_dir+f_wp, "r")
        raw_f = []
        for line in temp_f:
            raw_f.append(line)
        # skip the header, go directly to read the 1st bin.
        # Find the line that giving the 1st bin.
        bin_start_line = 0
        for line in raw_f:
            if "bin(" not in line:
                bin_start_line += 1
                continue
            else:
                break
        # start reading from first bin
        for line in raw_f[bin_start_line:]:
            if "bin(" in line:
                result = re.search('\((.*),', line).group(1)
                pt_bin = result
                pt_bin = convert_pt(pt_bin)
            if "central_value" in line:
                uncs[pt_bin][tag_weight][0] = line
            elif "sys" in line:
                unc_name = re.search('\((.*),', line).group(1)
                unc_name_set.add(unc_name)
                if uncs[pt_bin][tag_weight].has_key(1):
                    uncs[pt_bin][tag_weight][1].append(line)
                else:
                    uncs[pt_bin][tag_weight][1] = [line]
    temp_f.close()
    return uncs, list(unc_name_set)

# Find the maximum pt bin in normal CDI, which should be refpt bin
def get_ptref_bin_edge(raw_f_normal):
    lowpt_set = set()
    uppt_set = set()
    for line in raw_f_normal:
        if "<pt<" in line:
            lowpt = re.search('bin\((.*)<pt', line).group(1)
            uppt = re.search('pt<(.*?),', line).group(1)
            lowpt_set.add(int(lowpt))
            uppt_set.add(int(uppt))
    print("pt reference bin is: ("+str(max(lowpt_set))+\
              ", "+str(max(uppt_set))+")")
    return max(lowpt_set), max(uppt_set)

def get_ptref_uncs(raw_f_normal, label):
    ptref_bin_low, ptref_bin_up = get_ptref_bin_edge(raw_f_normal)
    ptref_uncs = defaultdict(dict)
    tagweight_key = ""
    for line in raw_f_normal:
        # ptref_uncs["WP"][0/1] 0: central_value 1: sys
        if "<pt<" in line:
            lowpt = re.search('bin\((.*)<pt', line).group(1)
            uppt = re.search('pt<(.*?),', line).group(1)
            if int(lowpt) == ptref_bin_low and int(uppt) == ptref_bin_up:
                # Hard coded so that abseta is always 2.5
                tagweight_key = re.search('abseta<2.5,(.*)\)', line).group(1)
            else:
                tagweight_key = ""
        if not tagweight_key:
            continue
        if "central_value" in line:
            if label != "l":
                ptref_uncs[tagweight_key][0] = line
            #------------------- rewrite stat unc as syst uncertainty -----------------------------
            else:
                unc, line = process_central_value(line)
                ptref_uncs[tagweight_key][0] = line
                light_stat = get_light_stat_unc(unc, tagweight_key)
                for line in light_stat:
                    if ptref_uncs[tagweight_key].has_key(1):
                        ptref_uncs[tagweight_key][1].append(line)
                    else:
                        ptref_uncs[tagweight_key][1] = [line]
            #------------------- rewrite stat unc as syst uncertainty -----------------------------
        elif "sys(" in line:
            if ptref_uncs[tagweight_key].has_key(1):
                ptref_uncs[tagweight_key][1].append(line)
            else:
                ptref_uncs[tagweight_key][1] = [line]
    return ptref_uncs

def get_light_stat_unc(unc, tagweight_key):
    tagweight_dict = {
        "tagweight<FixedCutBEff_85":"stat_FixedCut_85_100",
        "FixedCutBEff_85<tagweight<FixedCutBEff_77": "stat_FixedCut_77_85",
        "FixedCutBEff_77<tagweight<FixedCutBEff_70": "stat_FixedCut_70_77",
        "FixedCutBEff_70<tagweight<FixedCutBEff_60": "stat_FixedCut_60_70",
        "FixedCutBEff_60<tagweight":"stat_FixedCut_0_60"}
    result = []
    for tagweight in tagweight_dict:
        if tagweight == tagweight_key:
            tmp = "sys("+tagweight_dict[tagweight]+", "+unc+")\n"
        else:
            tmp = "sys("+tagweight_dict[tagweight]+", 0.0%)\n"
        result.append(tmp)
    return result

# Set the stat unc of central value to 0 and return that unc value
# for future use.
def process_central_value(line):
    left_end = 0
    unc = re.search(',(.*)\)', line).group(1)
    for i in range(len(line)):
        if line[i] == ",":
            line = line[:i+1] + "0.0%)\n"
            break
    return unc, line

def convert_WP(name):
    if name == "FixedCutBEff_60":
        return "FixedCutBEff_60<tagweight"
    elif name == "FixedCutBEff_70":
        return "FixedCutBEff_70<tagweight<FixedCutBEff_60"
    elif name == "FixedCutBEff_77":
        return "FixedCutBEff_77<tagweight<FixedCutBEff_70"
    elif name == "FixedCutBEff_85":
        return "FixedCutBEff_85<tagweight<FixedCutBEff_77"
    elif name == "FixedCutBEff_100":
        return "tagweight<FixedCutBEff_85"
    else:
        print("FixedCut working point "+name+\
                  " is not supported, Please check! Exiting")
        exit()

def convert_pt(name):
    lowpt = re.search('(.*)<pt', name).group(1)
    uppt = re.search('pt<(.*)', name).group(1)
    lowpt = str(int(float(lowpt)))
    uppt = str(int(float(uppt)))
    return lowpt + "<pt<" + uppt

def format_bin_line(line):
    if "\t" not in line:
        if ("bin(" in line or "{" in line or "}" in line or "meta_data" in line) and not "Analysis(" in line:
            line = "\t" + line
        elif "sys(" in line or "central_value" in line:
            line = "\t\t" + line
    if "\r" in line:
        line = line[:-2]+"\n"
    return line

# Get pt bin low edge from pt bin name
def get_lowpt(name):
    lowpt = re.search('(.*)<pt', name).group(1)
    return int(float(lowpt))

def write_new_file(input_dir, fname_normalCDI, extrap_uncs, extrap_unc_name_list, fname_output, label):
    f_normal = open(input_dir+fname_normalCDI, "r")
    f_out = open(fname_output,"w")
    raw_f_normal = []
    tagweight_key = ""
    
    for line in f_normal:
        raw_f_normal.append(line)
    ptref_uncs = get_ptref_uncs(raw_f_normal, label)
    tgw_name = ""
    for idx, line in enumerate(raw_f_normal):
        # stop before writing the end of the file
        if idx == len(raw_f_normal) - 1:
            break
        if "abseta<2.5" in line:
            tgw_name = re.search('abseta<2.5,(.*)\)', line).group(1)
        if "central_value" in line:
            #------------------- rewrite stat unc as syst uncertainty -----------------------------
            if label == "l":
                unc, line = process_central_value(line)
                f_out.write(format_bin_line(line))
                light_stat = get_light_stat_unc(unc, tgw_name)
                for line in light_stat:
                    f_out.write("\t\t"+line)
            #------------------- rewrite stat unc as syst uncertainty -----------------------------
            else:
                f_out.write(format_bin_line(line))
            for unc_name in extrap_unc_name_list:
                f_out.write("\t\tsys(EXTRAP_"+unc_name+",0.00%)\n")
        else:
            f_out.write(format_bin_line(line)) 

    tagweights = ["tagweight<FixedCutBEff_85", "FixedCutBEff_85<tagweight<FixedCutBEff_77", "FixedCutBEff_77<tagweight<FixedCutBEff_70", "FixedCutBEff_70<tagweight<FixedCutBEff_60", "FixedCutBEff_60<tagweight"]
    pt_bins = []
    ptreflow, ptrefup = get_ptref_bin_edge(raw_f_normal) 
    for pt_bin in extrap_uncs:
        if get_lowpt(pt_bin) >= ptrefup:
            pt_bins.append(pt_bin)
    pt_bins.sort(key = get_lowpt)
    for pt_bin in pt_bins:
        for tagweight in tagweights:            
            f_out.write("\tbin("+pt_bin+",0<abseta<2.5,"+tagweight+")\n")
            f_out.write("\t{\n")
            # Write central value
            #print(tagweight)
            #print(format_bin_line(ptref_uncs[tagweight][0]))
            f_out.write(format_bin_line(ptref_uncs[tagweight][0]))
            # Write extrapolation uncertainty
            for line in extrap_uncs[pt_bin][tagweight][1]:
                search_result = re.search('(.*)sys\((.*)', line)
                pre = search_result.group(1)
                post = search_result.group(2)
                f_out.write(pre+"sys(EXTRAP_"+post+"\n")
            # Write pt ref point uncertainty
            for line in ptref_uncs[tagweight][1]:
                f_out.write(format_bin_line(line))
            f_out.write("\t}\n")
    f_out.write("}\n")
    print("Finished combining CDI inputs.")
    return

def main():
    label = "l" # b,c,l
    input_dir = "/export/home/ke/yan/code_repo/high-pt-extrapolation-combinetxt/input/"+label+"jets/"

    bhad_post = ""
    if label == "b":
        fname_normalCDI = "btag_ttbarPDF_mc16ade_21-2-53_DL1r_FixedCutBEff_Continuous_vrtrackjets_BTagging201903.txt"
        bhad_post = "_bhad_unc"
    elif label == "l":
        fname_normalCDI = "NegTagZjets_AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903_DL1r_Continuous_CDI_v3.txt"
    elif label == "c":
        fname_normalCDI = "ttbarC_DL1rFixedCutBEff_VRJets_PFlow_Continuous_4bins_15_18_02Dec20.txt"
    else:
        print("Flavour "+label+" not supported! Exiting!")
        exit()
    
    fname_extrapCDI60 = "FixedCutBEff_60/MCcalibCDI_DL1r_extrap_FixedCutBEff_60_AntiKtVR30Rmax4Rmin02TrackJets_retagging_"+label+"_jet"+bhad_post+".txt"
    fname_extrapCDI70 = "FixedCutBEff_70/MCcalibCDI_DL1r_extrap_FixedCutBEff_70_AntiKtVR30Rmax4Rmin02TrackJets_retagging_"+label+"_jet"+bhad_post+".txt"
    fname_extrapCDI77 = "FixedCutBEff_77/MCcalibCDI_DL1r_extrap_FixedCutBEff_77_AntiKtVR30Rmax4Rmin02TrackJets_retagging_"+label+"_jet"+bhad_post+".txt"
    fname_extrapCDI85 = "FixedCutBEff_85/MCcalibCDI_DL1r_extrap_FixedCutBEff_85_AntiKtVR30Rmax4Rmin02TrackJets_retagging_"+label+"_jet"+bhad_post+".txt"
    fname_extrapCDI100 = "FixedCutBEff_100/MCcalibCDI_DL1r_extrap_FixedCutBEff_100_AntiKtVR30Rmax4Rmin02TrackJets_retagging_"+label+"_jet"+bhad_post+".txt"
    fname_output = "/export/home/ke/yan/code_repo/high-pt-extrapolation-combinetxt/output/"+fname_normalCDI
    print("Warnning! abseta is assumed to be 0<abseta<2.5 for all bins!!")
    extrap_uncs, extrap_unc_name_list = read_extrap_file(input_dir, fname_extrapCDI60, fname_extrapCDI70, fname_extrapCDI77, fname_extrapCDI85, fname_extrapCDI100)
    write_new_file(input_dir, fname_normalCDI, extrap_uncs, extrap_unc_name_list, fname_output, label)
    return

if __name__=="__main__":
    main()
