def read_file(fname):
    print("cache input")
    f = open(fname,"r")
    result = []
    for x in f:
        result.append(x)
    return result

def split_bin(fin, old_ptlow, old_ptup, new_ptlow_lst, new_ptup_lst):
    print("splitting bins and generate output")
    output = [] 
    length = len(fin)
    row = 0
    # Reading in header first
    for line in fin:
        if "<pt<" in line:
            break
        output.append(line)
        row += 1
    # Reading each pt tagweight bin 
    rows_to_cp = [0,0]
    old_pt_line = str(old_ptlow)+"<pt<"+str(old_ptup)
    while row < length:
        if old_pt_line in fin[row]:
            rows_to_cp[0] = row
            row += 1
            while not("<pt<" in fin[row]) and fin[row] != "}\n":
                row += 1
            row -= 1
            rows_to_cp[1] = row
            for new_ptlow, new_ptup in zip(new_ptlow_lst, new_ptup_lst):
                new_pt_line = str(new_ptlow)+"<pt<"+str(new_ptup)
                block = fin[rows_to_cp[0]: rows_to_cp[1]+1]
                block[0] = block[0].replace(old_pt_line, new_pt_line)
                output.extend(block)
            row += 1
            continue
        output.append(fin[row])
        row += 1
    return output        

def write_output(output, fname):
    print("writing output to file")
    fout = open(fname,"w")
    for line in output:
        fout.write(line)

def main():
    print("start splitting bins...")
    input_folder = "./input/cjets/"
    output_folder = "./output/cjet_splitpt/"
    fname = "ttbarC_DL1rFixedCutBEff_VRJets_PFlow_Continuous_4bins_15_18_02Dec20.txt"
    old_ptlow = 65
    old_ptup = 140
    new_ptlow = [65, 90, 115]
    new_ptup = [90, 115, 140]
    
    fin_vec = read_file(input_folder+fname)
    output = split_bin(fin_vec, old_ptlow, old_ptup, new_ptlow, new_ptup)
    write_output(output, output_folder+fname)

if __name__ == "__main__":
    main()
